using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrayerController : MonoBehaviour
{

    int left  = -1;
    int right = 1;
    int up    = 1;
    int down  = -1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //左矢印が押された時
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.Translate(left, 0, 0);　//左に「3」動かす
        }

        //右矢印が押された時
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.Translate(right, 0, 0);　//右に「3」動かす
        }

        //上矢印が押された時
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            transform.Translate(0, up, 0);　//上に「3」動かす
        }

        //下矢印が押された時
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            transform.Translate(0, down, 0);　//下に「3」動かす
        }

        if (transform.position.x == -10.0f) //左に行ける上限
        {
            left = 0;
        }
        else
        {
            left = -1;
        }

        if (transform.position.x == 9.0f)//右に行ける上限
        {
            right = 0;
        }
        else
        {
            right = 1;
        }

        if (transform.position.y == 4.0f)//上に行ける上限
        {
            up = 0;
        }
        else
        {
            up = 1;
        }

        if (transform.position.y == -4.0f)//下にいける上限
        {
            down = 0;
        }
        else
        {
            down = -1;
        }
    }
}
