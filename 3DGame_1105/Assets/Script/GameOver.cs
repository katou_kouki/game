using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    int ss = 0;
    public GameObject scoreObject;

    void Start()
    {
        //オブジェクトを名前で探す
        //GameObject resultObj = GameObject.Find("score");
        //変数「test」を参照しDebug.Logに出力
        //Debug.Log(resultObj.GetComponent<GameDirector>().test);

        //resultObj.transform.position = new Vector3(
        //resultObj.transform.position.x,
        //resultObj.transform.position.y - 5,
        //resultObj.transform.position.z );

        //「SCORE」というキーで保存されているInt値を読み込み
        int resultScore = PlayerPrefs.GetInt("SCORE");
        Debug.Log("保存されている点数：" + resultScore);

        int ss = resultScore;

        scoreObject.GetComponent<Text>().text = "SCORE " + ss;
    }

        // Update is called once per frame
        void Update()
    {
        
    }
}
