using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoodCubeController : MonoBehaviour
{
    GameObject player;

    //float good = -0.01f;

    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //フレームごとに等速で左に移動させる
        transform.Translate(-0.01f, 0, 0);

        //画面外に出たらオブジェクトを破棄する
        if (transform.position.x < -10.0f)
        {
            Destroy(gameObject);
        }

        //当たり判定
        Vector3 p1 = transform.position;             //矢の中心座標
        Vector3 p2 = this.player.transform.position; //プレイヤの中心座標
        Vector3 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.25f;//矢の半径
        float r2 = 0.7f;//プレイヤの半径


        if (d < r1 + r2)
        {
            //監督スクリプトにプレイヤと衝突したことを伝える
            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().ScoreUP();

            //衝突した場合は矢を消す
            Destroy(gameObject);
        }
    }
}
