using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    public GameObject HPObject;
    public GameObject scoreObject;

    int score = 0;
    int hp = 5;

    //public string test = "他のSceneで読み込まれるぜ！";

    void Start()
    {
        //DontDestroyOnLoad(this);

        //「SCORE」というキーで、Int値の「20」を保存

    }


    // Update is called once per frame
    void Update()
    {
        //this.hpGauge.GetComponent<Image>().fillAmount -= 0.1f;

        //if (score <= 30)
        //{
        //    Debug.Log("30koeta");
        //    GameObject director = GameObject.Find("BadCube");
        //    director.GetComponent<GameDirector>().Badss();
        //}
    }

    public void ScoreUP()
    {
        
        score++;
        scoreObject.GetComponent<Text>().text = "SCORE " + score;

        PlayerPrefs.SetInt("SCORE", score);
        PlayerPrefs.Save();
    }



    public void HP()
    {
        hp--;
        HPObject.GetComponent<Text>().text = "残機　" + hp;
        
        if (hp==0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }


}
