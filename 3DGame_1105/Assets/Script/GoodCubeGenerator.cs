using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoodCubeGenerator : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject GoodCubePrefab;
    float span = 0.5f;
    float delta = 0.0f;

    public GameObject BadCubePrefab;

    void Start()
    {

    }

    // Update is called once per frame
        void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
        GameObject go = Instantiate(GoodCubePrefab) as GameObject;
        int px = Random.Range(-4,5);
        go.transform.position = new Vector3(11, px, 0);

            GameObject goB = Instantiate(BadCubePrefab) as GameObject;
            int pxB = Random.Range(-4, 5);
            goB.transform.position = new Vector3(11, pxB, 0);
        }
    }
}
